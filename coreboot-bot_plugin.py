# -*- coding: utf-8 -*-
from irc3.plugins.command import command
from irc3.dec import event
from irc3 import rfc
import irc3

@irc3.plugin
class Plugin:

    botmsg = "Hi %s! Due to the recent events on the freenode network " + \
             "we moved to libera.chat %s. All our available community forums " + \
             "are listed on https://doc.coreboot.org/community/forums.html. " + \
             "See you on the other side!"

    def __init__(self, bot):
        self.bot = bot

    @event(rfc.JOIN)
    def say_hi(self, mask, channel, **kw):
        if mask.nick != self.bot.nick:
            self.sendmsg(mask.nick, channel)
        return

    @event(rfc.RPL_NAMREPLY)
    def names(self, channel=None, data=None, **kwargs):
        """Ping all users"""
        self.bot.privmsg(channel, data)
        self.sendmsg("all", channel)
        return

    def sendmsg(self, username, channel):
        self.bot.privmsg(channel, self.botmsg % (username, channel))
        return
