FROM archlinux:latest

RUN	pacman -Syu --noconfirm && \
	pacman -S --noconfirm python3 python-pip

RUN	groupadd bot && useradd -m -g bot -s /bin/bash bot
USER	bot
WORKDIR	/home/bot

RUN	pip3 install --user irc3

ADD	./config.ini ./
ADD	./coreboot-bot_plugin.py ./

CMD	python3 /home/bot/.local/bin/irc3 ./config.ini
